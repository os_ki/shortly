import express from "express";
import urlsRouter from "./api/urls.js";
import asyncMiddleware from "./middlewares/asyncMiddleware.js";
import { getOriginalUrl } from "./services/storage.js";

const app = express();

app.use("/api/urls", urlsRouter);

app.get(
  "/:key",
  asyncMiddleware(async (req, res) => {
    // GET for redirect to original URL
    const originalUrl = await getOriginalUrl(req.params.key);
    if (originalUrl) {
      return res.redirect(originalUrl);
    } else {
      return res.status(404).json({
        error: "Invalid key",
      });
    }
  })
);

export default app;
