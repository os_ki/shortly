/**
 * Catches errors happening inside async functions and
 * passes them to express error-handler
 * @param {function} fn
 */
const asyncMiddleware = (fn) => (req, res, next) => {
  Promise.resolve(fn(req, res, next)).catch(next);
};

export default asyncMiddleware;
