import { PORT } from "./config.js";
import app from "./app.js";
import { startCronJob } from "./services/cronJob.js";

app.listen(PORT, () => {
  startCronJob();
  console.log(`App is running at port ${PORT}`);
});
