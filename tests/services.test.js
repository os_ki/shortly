import { promises as fs } from "fs";
import path from "path";
import { STORAGE_FILE_PATH } from "../config.js";
import { cleanStorage } from "../services/cronJob.js";
import generateKey from "../services/generateKey.js";
import { createNewEntry, getOriginalUrl } from "../services/storage.js";

const DATA_FILE = path.resolve(STORAGE_FILE_PATH);

const testData = [
  {
    key: "qwerty",
    url: "https://hiqfinland.fi/avoimet-tyopaikat/",
  },
];

// Write testData to test storage-file before tests
beforeEach(async () => {
  await fs.writeFile(DATA_FILE, JSON.stringify(testData));
});

test("Create 10000 unique keys", async () => {
  // Create new array and fill with generated keys asynchronously
  const arr = await Promise.all(
    Array(10000)
      .fill("")
      .map(async (i) => await generateKey())
  );

  expect(isArrayUnique(arr)).toBeTruthy();
});

test("Create new entry", async () => {
  const testUrl = "https://www.youtube.com/watch?v=dQw4w9WgXcQ";
  const key = await createNewEntry(testUrl);
  expect(typeof key).toBe("string");
  const data = await fs.readFile(DATA_FILE);
  const createdRecord = JSON.parse(data).find((record) => record.key === key);
  // created record should be found from test storage-file
  expect(createdRecord.url).toBe(testUrl);
});

test("Get original url with key", async () => {
  const url = await getOriginalUrl(testData[0].key);
  expect(url).toBe(testData[0].url);
});

test("Clean old records from storage", async () => {
  // old record date: Sun Apr 11 2021 21:17:34 GMT+0300 (Eastern European Summer Time)
  const oldRecord = {
    key: "00000000kms236s2",
    url: "https://www.youtube.com/watch?v=dQw4w9WgXcQ",
  };
  const newRecord = {
    key: generateKey(),
    url: "https://hiqfinland.fi/avoimet-tyopaikat/",
  };
  await fs.writeFile(DATA_FILE, JSON.stringify([oldRecord, newRecord]));
  const data = await fs.readFile(DATA_FILE);
  expect(JSON.parse(data).length).toBe(2);
  await cleanStorage();
  const cleanedData = await fs.readFile(DATA_FILE);
  expect(JSON.parse(cleanedData).length).toBe(1);
});

// --- Helper functions ---
/**
 * Test if parameter is an array with unique values
 * @param {array} arr
 * @returns {boolean}
 */
function isArrayUnique(arr) {
  return Array.isArray(arr) && new Set(arr).size === arr.length;
}
