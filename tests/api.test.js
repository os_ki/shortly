import { promises as fs } from "fs";
import supertest from "supertest";
import path from "path";
import { STORAGE_FILE_PATH } from "../config.js";
import app from "../app.js";

const DATA_FILE = path.resolve(STORAGE_FILE_PATH);

const api = supertest(app);

const testData = [
  {
    key: "6310717dkn3hm042",
    url: "https://hiqfinland.fi/avoimet-tyopaikat/",
  },
];

// Write testData to test storage-file before tests
beforeEach(async () => {
  await fs.writeFile(DATA_FILE, JSON.stringify(testData));
});

//Get url for testData's first element
test("GET /api/urls/[valid key]", async () => {
  const response = await api.get(`/api/urls/${testData[0].key}`);
  expect(response.body.url).toBe(testData[0].url);
});

// Try to get non-existing record
test("GET /api/urls/not valid key", async () => {
  const response = await api.get(`/api/urls/not valid key`);
  expect(response.status).toBe(404);
});

test("POST /api/urls", async () => {
  const testUrl = "https://www.youtube.com/watch?v=dQw4w9WgXcQ";
  const response = await api.post("/api/urls").send({ url: testUrl });

  // api shoud return created key for the new record
  expect(typeof response.body.key).toBe("string");

  const data = await fs.readFile(DATA_FILE);
  const createdRecord = JSON.parse(data).find(
    (record) => record.key === response.body.key
  );
  // created record shoud be found from test storage-file
  expect(createdRecord.url).toBe(testUrl);
});
