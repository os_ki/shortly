import cron from "node-cron";
import { promises as fs } from "fs";
import path from "path";
import { STORAGE_FILE_PATH } from "../config.js";

const DATA_FILE = path.resolve(STORAGE_FILE_PATH);
// record is valid for 7 days
const RECORD_VALID_FOR = 1000 * 60 * 60 * 24 * 7;

function startCronJob() {
  // run cronJob every day at 00:00
  cron.schedule("0 0 * * *", async () => {
    cleanStorage();
  });
  // clean storage when app is started
  cleanStorage();
}

/**
 * Find records that are older than RECORD_VALID_FOR and remove them from storage
 */
const cleanStorage = async () => {
  const data = await fs.readFile(DATA_FILE);
  const filteredData = JSON.parse(data).filter((record) => {
    // record's creation time can be found from last 8 chars of the key
    const created = parseInt(record.key.slice(8), 36);
    // remove records created over 7 days ago
    return created + RECORD_VALID_FOR > Date.now();
  });
  if (data.length !== filteredData.length) {
    await fs.writeFile(DATA_FILE, JSON.stringify(filteredData));
  }
  return;
};

export { startCronJob, cleanStorage };
