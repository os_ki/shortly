import { randomBytes } from "crypto";

/**
 * Generates string of 16 characters. First 8 chars are randomly generated and following 8 chars contains information about when key was generated.
 * Note: datePart will remain 8 chars long till 25.5.2059. Come change your code before that if you still want your keys to be 16 characters long.
 * @returns {string}
 */
const generateKey = () => {
  const randomPart = randomBytes(4).toString("hex");
  const datePart = now().toString(36);
  return `${randomPart}${datePart}`;
};

// -- helpers --

/**
 * Returns numeric value of a date object.
 * Function keeps track of previous returned value to not return same value twice.
 * Note: Do not rely for this function to return the exact Date.now since if you call it multiple times at the exact time, it will add and keep adding one millisecond to the returned value.
 * @returns {number}
 */
function now() {
  const time = Date.now();
  let last = now.last || time;
  return (now.last = time > last ? time : last + 1);
}

export default generateKey;
