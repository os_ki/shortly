// https://nodejs.org/dist/latest-v14.x/docs/api/fs.html#fs_fs_promises_api
import { promises as fs } from "fs";
import { STORAGE_FILE_PATH } from "../config.js";
import path from "path";
import generateKey from "./generateKey.js";

const DATA_FILE = path.resolve(STORAGE_FILE_PATH);

/**
 * @param {string} key generated key of the original url
 * @returns {string|undefined} returns string if original url can be found, else will return undefined
 */
const getOriginalUrl = async (key) => {
  const data = await fs.readFile(DATA_FILE);
  const slugs = JSON.parse(data);
  const found = slugs.find((slug) => slug.key === key);
  return found?.url;
};

/**
 *
 * @param {string} url
 * @returns {string} generated key of the new entry
 */
const createNewEntry = async (url) => {
  const data = await fs.readFile(DATA_FILE);
  // generate key
  const key = generateKey();
  const slugs = JSON.parse(data);
  slugs.push({
    key,
    url,
  });
  await fs.writeFile(DATA_FILE, JSON.stringify(slugs));
  return key;
};

export { getOriginalUrl, createNewEntry };
