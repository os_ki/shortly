import express from "express";
import asyncMiddleware from "../middlewares/asyncMiddleware.js";
import { getOriginalUrl, createNewEntry } from "../services/storage.js";
import { PORT } from "../config.js";

const urlsRouter = express.Router();

// GET original url with key
urlsRouter.get(
  "/:key",
  asyncMiddleware(async (req, res) => {
    const url = await getOriginalUrl(req.params.key);
    return url
      ? res.json({ url })
      : res
          .status(404)
          .json({ error: `No results found with key: ${req.params.key}` });
  })
);

// POST for creating short url
urlsRouter.post(
  "/",
  express.json(),
  asyncMiddleware(async (req, res) => {
    // TODO: check that provided parameter is a valid url
    const { url } = req.body;
    if (!url) {
      return res.status(400).json({
        error: "Url-parameter missing",
      });
    }

    const key = await createNewEntry(url);

    return res.json({
      shortenedUrl: `http://localhost:${PORT}/${key}`,
      key,
    });
  })
);

export default urlsRouter;
