# Shortly

Simple express-app for creating and storing shortened urls.

## Getting started

> Project uses ECMAScript Modules so you need to have Node v14 installed.

Create `.env` -file. `> cp .env.example .env`  
Install dependencies `npm install`

## Development

`npm start` - starts the app  
`npm run dev` - starts the app with [nodemon](https://www.npmjs.com/package/nodemon) (restarts the app when file changes in the directory are detected)

## Production

Nope

## Tests

`./requests` -directory contains api requests to test with VS Code [REST Client](https://marketplace.visualstudio.com/items?itemName=humao.rest-client).

> Project is using [Jest](https://jestjs.io/) as testing library.

`./tests` -directory contains api/unit tests and `slugs.json` for storage.  
You can run all tests with `npm run test`

`api.tests.js` contains tests for api-endpoints with [supertest](https://www.npmjs.com/package/supertest). You can run these with `npm run test -- tests/api.test.js`

`services.test.js` contains unit-tests for functions inside `services` directory. You can run these with `npm run test -- tests/api.test.js`

## API

### GET `/:key`

Redirects to original url of the provided `key` -parameter.  
If original url is not found from storage, reponses with 404.

### GET `/api/urls/:key`

Returns original url of provided `key` -parameter.

### POST `/api/urls`

Expects request's body to contain `url` property.  
Creates new entry to storage and returns JSON-object

```json
{
  "shortenedUrl": "http://localhost:8000/f04476d6kn3j12uq",
  "key": "f04476d6kn3j12uq"
}
```

If request's `url` property is missing, responses with status `400`.

## Requirement specification

Create simple server with a REST-style API consisting of two endpoints.

- First endpoint takes in any url as parameter and returns shortened url from which the original can be retrieved.

> Example: `https://hiqfinland.fi/avoimet-tyopaikat/` => `http://localhost/agf4y0dmkgfs5xod`

- Second endpoint returns the original url the from shortened version

> Example: `http://localhost/agf4y0dmkgfs5xod` => `https://hiqfinland.fi/avoimet-tyopaikat/`

### Details and limitations

- Shortened urls must be unique
- Length of the unique key of the second endpoint is 16 characters. Unique key is the `agf4y0dmkgfs5xod` part of the example url.
- Shortened url is valid for 7 days
- Do not use a data storage which requires pre-installations to the computer running the code. E.g. do not use local mysql or mongodb to store the data.
- Write proper unit tests
- You can use any programming language you like
- Add source code to your personal online git repository
- Provide HiQ access for cloning the repository
- Provide instructions of how to run the code
- Follow the instructions based on you best understanding and fill in the details as you see fit.
