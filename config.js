import dotenv from "dotenv";
dotenv.config();

const PORT = process.env.PORT;

const STORAGE_FILE_PATH =
  process.env.NODE_ENV === "test"
    ? process.env.TEST_STORAGE_FILE_PATH
    : process.env.STORAGE_FILE_PATH;

export { PORT, STORAGE_FILE_PATH };
